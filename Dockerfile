# pull official base image
FROM python:3.10

# set work directory
RUN mkdir -p /app
WORKDIR /app

RUN mkdir -p /app/media
RUN mkdir -p /app/static

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependecies
COPY requirements.txt /app/
RUN pip install -r requirements.txt

# copy project
COPY . /app/